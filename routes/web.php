<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', ['uses' => 'HomeController@home', 'as' => 'front_end.home']);
Route::get('/shop', ['uses' => 'HomeController@getShop', 'as' => 'front_end.shop']);

Route::get('/cham-soc-cay', ['uses' => 'HomeController@getTakeCare', 'as' => 'front_end.take_care']);

Route::get('/lien-he', ['uses' => 'HomeController@getContact', 'as' => 'front_end.contact']);

Route::get('/cay/{slug}', ['uses' => 'HomeController@productDetail', 'as' => 'post.show']);

Auth::routes();

Route::group(['middleware' => 'auth', 'prefix' => 'p-admin'], function(){
    Route::get('/', function(){
        return redirect()->route('posts.index');
    });
    Route::resource('posts', 'PostController');
    Route::resource('categories', 'CategoryController');
    Route::resource('users', 'UserController');

});
