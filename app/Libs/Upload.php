<?php
/**
 * Created by PhpStorm.
 * User: dungpv
 * Date: 3/21/18
 * Time: 3:27 AM
 */

namespace App\Libs;


use Illuminate\Http\UploadedFile;

class Upload
{
    protected $file;
    protected $path;
    protected $fileName;
    public function __construct(UploadedFile $file, $path, $fileName = [])
    {
        $this->file = $file;
        $this->path = $path;
        $this->fileName = $fileName;
    }

    public function generateFileName()
    {
        return (!$this->fileName ? $this->file->getClientOriginalName() : str_slug(implode('-', $this->fileName))) . '.' . $this->getExtension();
    }

    public function getExtension(){
        return $this->file->getClientOriginalExtension();
    }
    public function upload()
    {
        $fileName = $this->generateFileName();
        $this->file->move(public_path($this->path), $fileName);
        return $fileName;
    }
}
