<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public static function getAll(){
        return self::orderBy('created_at', 'desc')->paginate(10);
    }

    public function insertUser($data){
        return (new self())->fill($data)->save();
    }

    public function deleteById($id){
        return self::findById($id)->delete();
    }

    public static function findById($id){
        return self::findOrFail($id);
    }

    public static function updateById($id, $data){
        return self::findById($id)->fill($data)->save();
    }
}
