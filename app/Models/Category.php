<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class Category extends Model
{
    use SoftDeletes;
    protected $table = 'categories';

    protected $fillable = ['name', 'description', 'slug'];
    public function posts(){

        return $this->hasMany(Post::class, 'category_id', 'id');
    }

    public static function getAll(){
        return self::paginate(10);
    }

    public static function insertCategory($data){
        return (new self())->fill($data)->save();
    }

    public static function updateById($id, $data){
        $category = self::findById($id);
        return $category->fill($data)->save();
    }

    public static function deleteById($id){
        $category = self::findOrFail($id);
        DB::transaction(function() use($category){
            $category->delete();
            $category->posts()->delete();
        });
        return;
    }

    public static function findById($id){
        return self::findOrfail($id);
    }

    public static function getSelectComboCategory(){
        return self::pluck('name', 'id');
    }

    public static function getAllWithProducts(){
        return self::with('posts')->get();
    }
}
