<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Post extends Model
{
    use SoftDeletes;

    protected $table = 'posts';
    protected $fillable = ['name', 'description', 'category_id', 'image_url', 'slug'];

    public function category()
    {

        return $this->belongsTo(Category::class, 'category_id', 'id');
    }


    public static function getAll($hasPaginate = true)
    {
        $query = self::with('category')->orderBy('created_at', 'desc');
        return $hasPaginate ? $query->paginate(10) : $query->get();
    }

    public static function findById($id){
        return self::findOrFail($id);
    }

    public function findBySlug($slug){
        return self::with('category')->whereSlug($slug)->first();
    }

    public static function insertPost($data){
        return (new self())->fill($data)->save();
    }

    public static function deleteById($id){
        return self::findById($id)->delete();
    }

    public function updateById($id, $data){
        $post = self::findById($id);
        $post->fill($data)->save();
        return $post;
    }

    public function getNextPost($id){
        $nextPostId = self::where('id', '>', $id)->min('id');
        return self::find($nextPostId);
    }

    public function getPreviousPost($id){
        $previousPostId = self::where('id', '<', $id)->max('id');
        return self::find($previousPostId);
    }

}
