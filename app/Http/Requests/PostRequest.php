<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'name' => 'required|min:3|max:255',
            'description' => 'required|max:300',
            'category_id' => 'required|integer',
            'image'=> 'mimes:jpg,jpeg,png,gif'
        ];
        $routeName = $this->route()->getName();
        if($routeName === 'posts.store'){
            $rules = array_merge($rules, ['image'=> 'required|mimes:jpg,jpeg,png,gif']);
        }
        return $rules;
    }

    public function messages()
    {
        return [
            'required' => ':attribute không được bỏ trống',
            'name:min' => 'Tên sản phẩm phải lớn hơn :min ký tự',
            'name:max' => 'Tên sản phẩm phải nhỏ hơn :max ký tự',
            'description.max' => 'Mô tả sản phẩm phải nhỏ hơn :max ký tự',
            'image.mimes' => 'Ảnh sản phẩm phải là định dạng jpg,jpeg,png,gif',
        ];
    }
}
