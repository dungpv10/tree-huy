<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'name' => 'required|min:3|max:100',
            'email' => 'required|email'
        ];
        $routeName = $this->route()->getName();

        return $routeName === 'users.update' ? $rules : array_merge($rules,
            [
                'password' => 'required',
                'email' => 'required|unique:users|email',
            ]
        );
    }
}
