<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Post;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    protected $post;
    protected $category;
    public function __construct(Post $post, Category $category)
    {
        $this->post = $post;
        $this->category = $category;
    }

    public function home(){
        $categories = $this->getPost();
        return view('front_end.home.index', compact('categories'));
    }


    public function productDetail($slug){
        $post = $this->post->findBySlug($slug);
        $nextPost = $this->post->getNextPost($post->id);
        $previousPost = $this->post->getPreviousPost($post->id);
        return view('front_end.product_detail.index', compact('post', 'nextPost', 'previousPost'));
    }
   public function getTakeCare(Request $request){

   }

   public function getContact(){

   }

   public function getShop(){

   }

   public function getPost(){

   }
}
