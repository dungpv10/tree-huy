<?php

namespace App\Http\Controllers;

use App\Http\Requests\PostRequest;
use App\Libs\Upload;
use App\Models\Category;
use App\Models\Post;
use Illuminate\Http\Request;

class PostController extends Controller
{
    protected $model;
    public function __construct(Post $model)
    {
        $this->model = $model;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = $this->model->getAll();
        return view('admin.posts.index', compact('posts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::getSelectComboCategory();
        return view('admin.posts.create', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PostRequest $request)
    {
        $this->upload($request);

        $this->model->insertPost($request->all());
        return redirect()->route('posts.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $post = $this->model->findById($id);
        $categories = Category::getSelectComboCategory();
        return view('admin.posts.edit', compact('post', 'categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(PostRequest $request, $id)
    {
        if($request->hasFile('image')){
            $this->upload($request);
        }

        $this->model->updateById($id, $request->all());
        return redirect()->route('posts.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->model->deleteById($id);
        return redirect()->route('posts.index');
    }

    private function mergeRequest(Request $request, $data){
        return $request->merge($data);
    }

    private function upload(Request $request){
        $slug = str_slug($request->get('name'));
        $uploadPath = 'uploads/posts';
        $fileName = (new Upload($request->file('image'),
            $uploadPath,
            [
                $request->get('name'),
                substr(time(), -5)
            ])
        )->upload();

        $this->mergeRequest($request,
            [
                'image_url' => $uploadPath . '/' . $fileName,
                'slug' => $slug
            ]
        );
        return $request;
    }
}
