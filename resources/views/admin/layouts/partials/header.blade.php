<header class="header">
    <a href="/" class="logo">
        <img src="/images/logo_luatdan.png" style="max-width: 45px;" alt="logo">
    </a>
    <nav class="navbar navbar-static-top" role="navigation">
        <!-- Sidebar toggle button-->
        <div>
            <a href="#" class="navbar-btn sidebar-toggle" data-toggle="offcanvas" role="button">
                <div class="responsive_nav"></div>
            </a>
        </div>
        <div class="navbar-right">
            <ul class="nav navbar-nav">

                <li class="dropdown user user-menu">

                        <div class="riot">
                            <div>
                                {!! Form::open(['route' => 'logout', 'method' => 'POST']) !!}
                                    {!! Form::submit('Đăng xuất', ['class' => 'btn btn-primary']) !!}
                                {!! Form::close() !!}
                            </div>
                        </div>


                </li>
            </ul>
        </div>
    </nav>
</header>
