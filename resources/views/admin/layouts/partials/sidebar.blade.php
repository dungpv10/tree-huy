<aside class="left-side sidebar-offcanvas">
    <section class="sidebar ">
        <div class="page-sidebar  sidebar-nav">

            <div class="clearfix"></div>
            <!-- BEGIN SIDEBAR MENU -->
            <ul class="page-sidebar-menu" id="menu">
                <li class="@if(Request::is('p-admin/categories') || Request::is('p-admin/categories/*')) active @endif">
                    <a href="#">
                        <i class="livicon" data-name="medal" data-size="18" data-c="#00bc8c" data-hc="#00bc8c" data-loop="true"></i>
                        <span class="title">Danh mục</span>
                        <span class="fa arrow"></span>
                    </a>
                    <ul class="sub-menu">
                        <li>
                            <a href="{{ route('categories.index') }}">
                                <i class="fa fa-angle-double-right"></i>
                                Danh sách
                            </a>
                        </li>
                        <li>
                            <a href="{{ route('categories.create') }}">
                                <i class="fa fa-angle-double-right"></i>
                                Tạo mới
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="@if(Request::is('p-admin/posts') || Request::is('p-admin/posts/*')) active @endif">
                    <a href="#">
                        <i class="livicon" data-name="medal" data-size="18" data-c="#00bc8c" data-hc="#00bc8c" data-loop="true"></i>
                        <span class="title">Sản phẩm</span>
                        <span class="fa arrow"></span>
                    </a>
                    <ul class="sub-menu">
                        <li>
                            <a href="{{ route('posts.index') }}">
                                <i class="fa fa-angle-double-right"></i>
                                Danh sách
                            </a>
                        </li>
                        <li>
                            <a href="{{ route('posts.create') }}">
                                <i class="fa fa-angle-double-right"></i>
                                Tạo mới
                            </a>
                        </li>
                    </ul>
                </li>

                <li class="@if(Request::is('p-admin/users') || Request::is('p-admin/users/*')) active @endif">
                    <a href="#">
                        <i class="livicon" data-name="medal" data-size="18" data-c="#00bc8c" data-hc="#00bc8c" data-loop="true"></i>
                        <span class="title">Thành viên</span>
                        <span class="fa arrow"></span>
                    </a>
                    <ul class="sub-menu">
                        <li>
                            <a href="{{ route('users.index') }}">
                                <i class="fa fa-angle-double-right"></i>
                                Danh sách
                            </a>
                        </li>
                        <li>
                            <a href="{{ route('users.create') }}">
                                <i class="fa fa-angle-double-right"></i>
                                Tạo mới
                            </a>
                        </li>
                    </ul>
                </li>

            </ul>
            <!-- END SIDEBAR MENU -->
        </div>
    </section>
    <!-- /.sidebar -->
</aside>
