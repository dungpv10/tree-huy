<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>@yield('title')</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond./admin/js/1.3.0/respond.min.js"></script>
    <![endif]-->
    <!-- global css -->
    <link href="/admin/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="/admin/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="/admin/css/styles/black.css" rel="stylesheet" type="text/css" id="colorscheme" />
    <link href="/admin/css/panel.css" rel="stylesheet" type="text/css"/>
    <link href="/admin/css/metisMenu.css" rel="stylesheet" type="text/css"/>

    <!-- end of global css -->
    <!-- page level css -->

    @yield('css_header')
    <!--end of page level css-->
</head>

<body class="skin-josh">
    @include('admin.layouts.partials.header')
<div class="wrapper row-offcanvas row-offcanvas-left">
    <!-- Left side column. contains the logo and sidebar -->
@include('admin.layouts.partials.sidebar')
    <!-- Right side column. Contains the navbar and content of the page -->
    <aside class="right-side">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <!--section starts-->

        </section>
        <!--section ends-->
        <section class="content">
            @yield('content')

        </section>
        <!-- content -->
    </aside>
    <!-- right-side -->
</div>
<!-- ./wrapper -->
<a id="back-to-top" href="#" class="btn btn-primary btn-lg back-to-top" role="button" title="Return to top" data-toggle="tooltip" data-placement="left">
    <i class="livicon" data-name="plane-up" data-size="18" data-loop="true" data-c="#fff" data-hc="white"></i>
</a>
<!-- global js -->
<script src="/admin/js/jquery-1.11.3.min.js" type="text/javascript"></script>
<script src="/admin/js/bootstrap.min.js" type="text/javascript"></script>
<!--livicons-->
<script src="/admin/vendors/livicons/minified/raphael-min.js" type="text/javascript"></script>
<script src="/admin/vendors/livicons/minified/livicons-1.4.min.js" type="text/javascript"></script>
<script src="/admin/js/josh.js" type="text/javascript"></script>
<script src="/admin/js/metisMenu.js" type="text/javascript"> </script>
<script src="/admin/vendors/holder/holder.js" type="text/javascript"></script>
<!-- end of global js -->

@yield('footer_js')
</body>
</html>
