@extends('admin.layouts.main')
@section('css_header')
    <link href="/admin/vendors/jasny-bootstrap/jasny-bootstrap.min.css" rel="stylesheet" />
@stop
@section('content')
    <div class="row">

        <div class="col-md-8 col-md-offset-2">
            <!--md-6 starts-->
            <!--form control starts-->
            <div class="panel panel-success" id="hidepanel6">
                <div class="panel-heading">
                    <h3 class="panel-title">
                        <i class="livicon" data-name="share" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i>
                        Tạo mới sản phẩm
                    </h3>
                    <span class="pull-right">
                                    <i class="glyphicon glyphicon-chevron-up clickable"></i>
                                    <i class="glyphicon glyphicon-remove removepanel clickable"></i>
                                </span>
                </div>
                <div class="panel-body">
                    <div class="row">
                        @foreach($errors->all() as $error)

                            <div class="col-sm-6 padding">
                                <span id="8char" class="glyphicon glyphicon-remove color-pwd"></span> {!! $error !!}
                            </div>
                        @endforeach
                    </div>
                    {!! Form::open(['route' => 'posts.store', 'method' => 'POST', 'enctype' => 'multipart/form-data']) !!}

                        <div class="form-group">
                            <label>Tên sản phẩm</label>
                            {!! Form::text('name', '', ['class' => 'form-control']) !!}
                        </div>
                        <div class="form-group">
                            <label>Mô tả</label>
                            {!! Form::textarea('description', '', ['class' => 'form-control', 'rows' => 3]) !!}
                        </div>



                        <div class="form-group">
                            <label>Chọn danh mục</label>
                            {!! Form::select('category_id', $categories, array_keys($categories->toArray())[0], ['class' => 'form-control']) !!}
                        </div>

                    <div class="form-group">
                        <div class="fileinput fileinput-new" data-provides="fileinput">
                            <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                <img data-src="holder.js/100%x100%" alt="..."></div>
                            <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"></div>
                            <div>
                                                <span class="btn btn-default btn-file">
                                                    <span class="fileinput-new">Chọn ảnh sản phẩm</span>
                                                    <span class="fileinput-exists">Thay đổi</span>
                                                    {!! Form::file('image') !!}

                                                </span>
                                <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Xoá</a>
                            </div>
                        </div>
                    </div>
                    {!! Form::submit('Tạo mới sản phẩm', ['class' => 'btn btn-responsive btn-primary']) !!}
                    {!! Form::reset('Nhập lại', ['class' => 'btn btn-responsive btn-default']) !!}


                    {!! Form::close() !!}
                </div>
            </div>
        </div>

    </div>

@stop

@section('footer_js')

    <script src="/admin/vendors/jasny-bootstrap/jasny-bootstrap.min.js"></script>
@stop
