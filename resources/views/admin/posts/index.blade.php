@extends('admin.layouts.main')

@section('css_header')
    <link href="/admin/css/pages/tables.css" rel="stylesheet" type="text/css" />
@stop
@section('content')
    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN SAMPLE TABLE PORTLET-->
            <div class="portlet box primary">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="livicon" data-name="camera-alt" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i>
                        Danh sách sản phẩm
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="table-scrollable">
                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Ảnh sản phẩm</th>
                                <th>Tên sản phẩm</th>
                                <th>URL</th>
                                <th>Mô tả</th>
                                <th>Thao tác</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($posts as $post)
                                <tr>
                                <td>{{ $post->id }}</td>
                                <td><img src="/{{ $post->image_url }}" style="max-width: 200px;"/></td>
                                <td>{{ $post->name }}</td>
                                <td>{{ $post->slug }}</td>
                                <td>{{ $post->description }}</td>
                                <td>
                                    <a href="{{ route('posts.edit', [$post->id]) }}" class="btn btn-sm btn-success">Sửa</a>
                                    {!! Form::open(['route' => ['posts.destroy', $post->id], 'method' => 'delete']) !!}
                                    {!! Form::submit('Xoá', ['class' => 'btn btn-sm btn-danger']) !!}
                                    {!! Form::close() !!}
                                </td>
                            </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div>
                        {!! $posts !!}
                    </div>
                </div>
            </div>
            <!-- END SAMPLE TABLE PORTLET-->
        </div>
    </div>
@stop
