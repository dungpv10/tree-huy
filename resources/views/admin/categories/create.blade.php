@extends('admin.layouts.main')
@section('css_header')
    <link href="/admin/vendors/jasny-bootstrap/jasny-bootstrap.min.css" rel="stylesheet" />
    @stop
@section('content')
    <div class="row">

        <div class="col-md-8 col-md-offset-2">
            <!--md-6 starts-->
            <!--form control starts-->
            <div class="panel panel-success" id="hidepanel6">
                <div class="panel-heading">
                    <h3 class="panel-title">
                        <i class="livicon" data-name="share" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i>
                        Tạo mới danh mục
                    </h3>
                </div>
                <div class="panel-body">
                    <div class="row">
                        @foreach($errors->all() as $error)

                            <div class="col-sm-6 padding">
                                <span id="8char" class="glyphicon glyphicon-remove color-pwd"></span> {!! $error !!}
                            </div>
                        @endforeach
                    </div>
                    {!! Form::open(['route' => 'categories.store', 'method' => 'post']) !!}
                        <div class="form-group">
                            <label>Tên danh mục</label>
                            {!! Form::text('name', '', ['class' => 'form-control']) !!}
                        </div>
                        <div class="form-group">
                            <label>Mô tả</label>
                            {!! Form::textarea('description','', ['class' => 'form-control']) !!}
                        </div>
                        <button type="submit" class="btn btn-responsive btn-primary">Tạo mới</button>
                        <button type="reset" class="btn btn-responsive btn-default">Nhập lại</button>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>

    </div>

    @stop

@section('footer_js')

    <script src="/admin/vendors/jasny-bootstrap/jasny-bootstrap.min.js"></script>
    @stop
