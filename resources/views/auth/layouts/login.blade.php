<!DOCTYPE html>
<html>

<head>
    <title>Admin Login | Josh Admin Template</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- global level css -->
    <link href="/admin/css/bootstrap.min.css" rel="stylesheet" />
    <!-- end of global level css -->
    <!-- page level css -->
    <link href="/admin/css/pages/login2.css" rel="stylesheet" />
    <link href="/admin/vendors/iCheck/skins/minimal/blue.css" rel="stylesheet" />
    <!-- styles of the page ends-->
</head>

<body>
<div class="container">
    <div class="row vertical-offset-100">
        <div class=" col-xs-10 col-xs-offset-1 col-sm-6 col-sm-offset-3  col-md-5 col-md-offset-4 col-lg-4 col-lg-offset-4">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title text-center">Sign In</h3>
                </div>
                <div class="panel-body">
                    {!! Form::open(['route' => 'login', 'method' => 'POST']) !!}

                        <fieldset>
                            <div class="form-group input-group">
                                <div class="input-group-addon">
                                    <i class="livicon" data-name="at" data-size="18" data-c="#484848" data-hc="#484848" data-loop="true"></i>
                                </div>
                                <input class="form-control" placeholder="E-mail" name="email" type="text"  required>
                            </div>
                            <div class="form-group input-group">
                                <div class="input-group-addon">
                                    <i class="livicon" data-name="key" data-size="18" data-c="#484848" data-hc="#484848" data-loop="true"></i>
                                </div>
                                <input class="form-control" placeholder="Password" name="password" type="password" value="" required>
                            </div>
                            {!! Form::submit('Login', ['class' => 'btn btn-lg btn-primary btn-block']) !!}
                        </fieldset>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
<!-- global js -->
<script src="/admin/js/jquery-1.11.3.min.js" type="text/javascript"></script>
<script src="/admin/js/bootstrap.min.js" type="text/javascript"></script>
<!--livicons-->
<script src="/admin/vendors/livicons/minified/raphael-min.js" type="text/javascript"></script>
<script src="/admin/vendors/livicons/minified/livicons-1.4.min.js" type="text/javascript"></script>
<!-- end of global js -->
<!-- begining of page level js-->
<script src="/admin/js/TweenLite.min.js"></script>
<script src="/admin/vendors/iCheck/icheck.js" type="text/javascript"></script>
<script src="/admin/js/pages/login2.js" type="text/javascript"></script>
<!-- end of page level js-->
</body>
</html>
