@extends('front_end.layouts.main')
@section('title') {{ $post->name }} - Luatdanplastic.com  @stop
@section('main_content')
    <div id="fh5co-main">

        <div class="fh5co-narrow-content">
            <div class="row">

                <div class="col-md-12 animate-box" data-animate-effect="fadeInLeft">
                    <figure class="text-center">
                        <img style="width: 100%;" src="/{{ $post->image_url }}" alt="{{ $post->name }}" class="img-responsive">
                    </figure>
                </div>

                <div class="col-md-8 col-md-offset-2 animate-box" data-animate-effect="fadeInLeft">

                    <div class="col-md-7 col-md-push-3">
                        <h1>{{ $post->name }}</h1>

                        <p>{!! $post->description !!}</p>
                    </div>

                    <div class="col-md-5 col-md-pull-9 fh5co-services">
                        <h3>{{ $post->category->name }}</h3>
                        <ul>
                            <li>{{ $post->category->description }}</li>
                        </ul>
                    </div>

                </div>
            </div>

            <div class="row work-pagination animate-box" data-animate-effect="fadeInLeft">
                <div class="col-md-8 col-md-offset-2 col-sm-12 col-sm-offset-0">
                    @if($previousPost)
                        <div class="col-md-4 col-sm-4 col-xs-4 text-center">
                            <a href="{{ route('post.show', $previousPost->slug) }}"><i class="icon-long-arrow-left"></i> <span>Sản phẩm sau</span></a>
                        </div>
                    @endif
                    <div class="col-md-4 col-sm-4 col-xs-4 text-center">

                    </div>
                    @if($nextPost)
                        <div class="col-md-4 col-sm-4 col-xs-4 text-center">
                            <a href="{{ route('post.show', $nextPost->slug) }}"><span>Sản phẩm kế</span> <i class="icon-long-arrow-right"></i></a>
                        </div>
                    @endif
                </div>
            </div>

        </div>
        @include('front_end.includes.service')
    </div>
@stop
