<div class="fh5co-narrow-content">
    <h2 class="fh5co-heading animate-box text-uppercase" data-animate-effect="fadeInLeft">Các sản phẩm chính</h2>
    <div class="row animate-box" data-animate-effect="fadeInLeft">
        @foreach($posts as $post)
            @include('front_end.includes.product_single')
            <div class="clearfix visible-sm-block"></div>
        @endforeach
    </div>
</div>
