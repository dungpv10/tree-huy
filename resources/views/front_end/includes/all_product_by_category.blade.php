<div class="fh5co-narrow-content">
    <h2 class="fh5co-heading animate-box text-uppercase" data-animate-effect="fadeInLeft">Các sản phẩm chính</h2>
    @foreach($categories as $category)
        <div class="row animate-box" data-animate-effect="fadeInLeft" style="padding: 0 15px;">
            <h3 style="color: #da1212">{{ $category->name }}</h3>
            <div>
                @foreach($category->posts as $post)
                    @include('front_end.includes.product_single')
                @endforeach
            </div>
                <div class="clearfix visible-sm-block"></div>
        </div>

    @endforeach
</div>
