<div
        class="fh5co-narrow-content animate-box fadeInLeft animated"
        data-animate-effect="fadeInLeft"
        style="width: 100%;padding: 20px;background-color: #3D3D3D;color: #fff;font-size: 15px;"
>

    <form action="">
        <div class="row">
            <div class="col-md-12">
                <div class="row" style="width: 80%;margin:0 auto;">
                    <div class="col-md-6" style="margin-bottom : 30px;">
                        <h3 style="margin: 0;color: #fff;font-weight: 600;font-size: 1em;text-transform: uppercase">Chăm sóc khách hàng</h3><br/>
                        - Bộ phận chăm sóc khách hàng
                        <div>
                            <span>
                                <i class="fa fa-phone fa- "></i> Ms. Hiền - 0971 193 399
                            </span>
                        </div>
                        <div>
                            <span>
                                <i class="fa fa-phone fa- "></i> Mr. Lực - 0964 686 893
                            </span>
                        </div>
                    </div>
                    <div class="col-md-6" style="margin-bottom : 30px;">
                        <h3 style="margin: 0;color: #fff;font-weight: 600;font-size: 1em;text-transform: uppercase">Thông tin liên lạc</h3><br/>
                        <div>
                            <span>
                                <i class="fa fa-map-marker "></i> Khu công nghiệp làng nghề Minh Khai, Như Quỳnh, Văn Lâm, Hưng Yên
                            </span>
                        </div>
                        <div>
                            <span>
                                <i class="fa fa-phone fa- "></i> Mr. Lực - 0964 686 893
                            </span>
                        </div>
                        <div>
                            <span>
                                <i class="fa fa-envelope-o"></i> luatdanplastic@gmail.com
                            </span>
                        </div>
                    </div>

                </div>
            </div>

        </div>
    </form>
</div>
