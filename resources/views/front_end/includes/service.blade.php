<div class="fh5co-narrow-content">
    <h2 class="fh5co-heading animate-box" data-animate-effect="fadeInLeft">Định hướng <span>Và phát triển</span></h2>
    <div class="row">
        <div class="col-md-6 service">
            <div class="fh5co-feature animate-box" data-animate-effect="fadeInLeft">
                <div class="fh5co-icon">
                    <i class="icon-strategy"></i>
                </div>
                <div class="fh5co-text">
                    <h3>Chiến lược</h3>
                    <p>Luôn luôn trang bị công nghệ tiên tiến nhất nhằm sản xuất ra những sản phẩm đạt tiêu chuẩn quốc tế và phù hợp với đa số người tiêu dùng. </p>
                </div>
            </div>
        </div>
        <div class="col-md-6 service">
            <div class="fh5co-feature animate-box " data-animate-effect="fadeInLeft">
                <div class="fh5co-icon">
                    <i class="icon-telescope"></i>
                </div>
                <div class="fh5co-text">
                    <h3>Định hướng</h3>
                    <p>Đến năm 2020 là nhà máy sản xuất các sản phẩm về nhựa lớn nhất miền bắc và vươn ra cả thế giới. </p>
                </div>
            </div>
        </div>

        <div class="col-md-6 service">
            <div class="fh5co-feature animate-box" data-animate-effect="fadeInLeft">
                <div class="fh5co-icon">
                    <i class="icon-circle-compass"></i>
                </div>
                <div class="fh5co-text">
                    <h3>Chăm sóc khách hàng</h3>
                    <p>Luôn sẵn sàng đáp ứng mọi nhu cầu của khách hàng về chất lượng, số lượng và tiến độ giao hàng. </p>
                </div>
            </div>
        </div>
        <div class="col-md-6 service">
            <div class="fh5co-feature animate-box" data-animate-effect="fadeInLeft">
                <div class="fh5co-icon">
                    <i class="icon-tools-2"></i>
                </div>
                <div class="fh5co-text">
                    <h3>Đảm bảo</h3>
                    <p>Luôn đảm bảo yêu cầu về uy tín, chất lượng tốt nhất đến với khách hàng. </p>
                </div>
            </div>
        </div>

    </div>
</div>
