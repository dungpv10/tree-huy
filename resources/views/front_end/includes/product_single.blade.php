<div class="col-md-4 col-sm-6 col-xs-6 col-xxs-12 work-item">
    <a href="{{ route('post.show', $post->slug) }}">
        <img src="{{ $post->image_url }}" alt="" class="img-responsive">
        <h3 class="fh5co-work-title">{{ $post->name }}</h3>
        <p>{{ $post->category->name }}</p>
    </a>
</div>
