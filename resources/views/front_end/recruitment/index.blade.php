@extends('front_end.layouts.main')
@section('title') Tuyển dụng - Luatdanplastic.com  @stop
@section('main_content')
    <div id="fh5co-main">

        <div class="fh5co-narrow-content">
            <div class="row">
                <div class="col-md-12">
                    <h2 class="fh5co-heading animate-box text-uppercase" data-animate-effect="fadeInLeft">Tuyển dụng</h2>
                    <p class="fh5co-lead animate-box" data-animate-effect="fadeInLeft">
                        <h3>Tuyển Lao động phổ thông</h3>
                        Cơ sở sản xuất nhựa Luật Đàn chuyên sản xuất cốc, nắp, bát,
                        đĩa, khay bánh kẹo, sản phẩm định hình... Với nguồn nguyên
                        liệu hạt nhựa nguyên sinh nhập khẩu 100% và hệ thống dây
                        chuyền sản xuất hiện đại.
                    </p>

                    <p class="fh5co-lead animate-box" data-animate-effect="fadeInLeft">
                        <h3>Yêu cầu</h3>
                        <div>- Có sức khỏe tốt, trung thực</div>
                        <div>- Tuổi từ 18 đến 35.</div>
                    </p>
                    <p class="fh5co-lead animate-box" data-animate-effect="fadeInLeft">
                        <h3>Quyền lợi</h3>
                        <div>- Lương cơ bản + Lương năng suất (Theo đối
                            tượng công nhân)</div>
                        <div>- Công ty trợ cấp 1 bữa ăn trưa tại nhà máy</div>
                        <div>- Phụ cấp thêm giờ, tăng ca.</div>
                    </p>
                    <p class="fh5co-lead animate-box" data-animate-effect="fadeInLeft">
                        <h3>Hình thức làm việc</h3>
                        <div>- Theo ca</div>

                    </p>

                    <p class="fh5co-lead animate-box" data-animate-effect="fadeInLeft">
                        <h3>Hồ sơ ứng tuyển bao gồm</h3>
                        <div>1. Sơ yếu lý lịch(có xác nhận của địa phương)</div>
                        <div>2. Bản sao công chứng các bằng cấp trình độ chuyên môn</div>
                        <div>3. Bản sao phô tô công chứng CMND và sổ hộ khẩu</div>
                        <div>3. Bản sao giấy khai sinh</div>
                        <div>3. 02 ảnh 3x4</div>
                        <div>3. Đơn xin việc</div>
                        <div>3. Giấy khám sức khỏe</div>
                    </p>

                    <p class="fh5co-lead animate-box" data-animate-effect="fadeInLeft">
                        <h3>Địa điểm làm việc</h3>
                        <div>- Cơ sở sản xuất nhựa Luật Đàn</div>
                        <div>- Khu công nghiệp làng nghề Minh Khai – Như Quỳnh – Văn
                            Lâm – Hưng Yên</div>
                    </p>

                    <p class="fh5co-lead animate-box" data-animate-effect="fadeInLeft">
                        <h3>Chi tiết liên hệ</h3>
                        <div>Mr.Lực : 0964 686 893</div>

                    </p>

                </div>

            </div>

        </div>



        @include('front_end.includes.service')

    </div>
@stop
