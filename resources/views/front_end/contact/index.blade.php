@extends('front_end.layouts.main')
@section('title') Liên hệ - Luatdanplastic.com  @stop
@section('header_css')
    <style>
        .adds-luat-dan li a{
            font-size: 14px;
        }
        .nav-tabs > li > a{
            border-radius: 0;
        }
        .nav-tabs > li.active > a, .nav-tabs > li.active > a:hover, .nav-tabs > li.active > a:focus{
            background-color: #da1212;
            color: #fff;
        }
    </style>
@stop
@section('main_content')
    <div id="fh5co-main">
        <ul class="nav nav-tabs adds-luat-dan">
            <li class="active"><a data-toggle="tab" href="#home">Cơ sở sản xuất cốc nhựa Luật Đàn</a></li>
            <li><a data-toggle="tab" href="#menu1">Xưởng sản xuất cốc nhựa Luật Đàn cơ sở 2</a></li>
            <li><a data-toggle="tab" href="#menu2">Xưởng sản xuất cốc nhựa Luật Đàn cơ sở 3</a></li>
        </ul>

        <div class="tab-content">
            <div id="home" class="tab-pane fade in active" style="width: 100%;min-height: 100vh">
                <iframe style="width:100%;height: -webkit-fill-available;border:0"
                        src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3725.027329292232!2d105.98048381493204!3d20.991541586018528!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3135a5e3242d89a3%3A0x62f72de3d4d09cb7!2zQ8ahIHPhu58gc-G6o24geHXhuqV0IG5o4buxYSBMdeG6rXQgxJDDoG4!5e0!3m2!1svi!2s!4v1521731928680" width="100%" height="100%" frameborder="0" allowfullscreen></iframe>
            </div>
            <div id="menu1" class="tab-pane fade">
                <iframe style="width:100%;height: -webkit-fill-available;border:0" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3725.0072078653216!2d105.97794911493207!3d20.992348186018113!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3135a5fd1fa5e0b9%3A0xc3dc66df23704f93!2zWMaw4bufbmcgc-G6o24geHXhuqV0IGPhu5FjIG5o4buxYSBMdeG6rXQgxJDDoG4gY8ahIHPhu58gMg!5e0!3m2!1svi!2s!4v1522002516883" width="100%" height="100%" frameborder="0" allowfullscreen></iframe>
            </div>
            <div id="menu2" class="tab-pane fade">
                <iframe style="width:100%;height: -webkit-fill-available;border:0"
                        src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1206.931352759745!2d105.98283899999998!3d20.990070399999997!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x0!2zMjDCsDU5JzI0LjAiTiAxMDXCsDU4JzU5LjUiRQ!5e0!3m2!1svi!2s!4v1522045614951" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
            </div>

        </div>


        {{--<div style="width: 100%;min-height: 100vh">--}}
            {{--<iframe style="width:100%;height: -webkit-fill-available;border:0"--}}
                    {{--src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3725.027329292232!2d105.98048381493204!3d20.991541586018528!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3135a5e3242d89a3%3A0x62f72de3d4d09cb7!2zQ8ahIHPhu58gc-G6o24geHXhuqV0IG5o4buxYSBMdeG6rXQgxJDDoG4!5e0!3m2!1svi!2s!4v1521731928680" width="100%" height="100%" frameborder="0" allowfullscreen></iframe>--}}
        {{--</div>--}}

        <div class="fh5co-more-contact">
            <div class="fh5co-narrow-content">
                <div class="row">
                    <div class="col-md-4">
                        <div class="fh5co-feature fh5co-feature-sm animate-box" data-animate-effect="fadeInLeft">
                            <div class="fh5co-icon">
                                <i class="icon-envelope-o"></i>
                            </div>
                            <div class="fh5co-text">
                                <p><a href="#">luatdanplastic@gmail.com</a></p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="fh5co-feature fh5co-feature-sm animate-box" data-animate-effect="fadeInLeft">
                            <div class="fh5co-icon">
                                <i class="icon-map-o"></i>
                            </div>
                            <div class="fh5co-text">
                                <p><a href="#">Khu công nghiệp Làng Nghề Minh Khai – Như Quỳnh
                                        – Văn Lâm – Hưng Yên</a></p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="fh5co-feature fh5co-feature-sm animate-box" data-animate-effect="fadeInLeft">
                            <div class="fh5co-icon">
                                <i class="icon-phone"></i>
                            </div>
                            <div class="fh5co-text">
                                <p><a href="">0964 686 893<br/> 0971 193 399</a></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @include('front_end.includes.service')

    </div>
@stop

@section('footer_js')

    <script async defer
            src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDxplWTAjMsb5Z3K9nuHGZWXREfa-a_WpU&callback=initMap"></script>

@stop
