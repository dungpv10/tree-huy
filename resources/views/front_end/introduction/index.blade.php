@extends('front_end.layouts.main')
@section('title') Giới thiệu - Luatdanplastic.com  @stop
@section('main_content')
    <div id="fh5co-main">

        <div class="fh5co-narrow-content">
            <div class="row">
                <div class="col-md-12">
                    <h2 class="fh5co-heading animate-box text-uppercase" data-animate-effect="fadeInLeft">Về chúng tôi</h2>
                    <p class="fh5co-lead animate-box" data-animate-effect="fadeInLeft">Cơ sở sản xuất nhựa Luật Đàn với phương châm luôn lắng nghe
                        ý kiến khách hàng, uy tín, tận tâm trong công việc kinh doanh
                        và sản xuất. Chúng tôi luôn xem sự thành công của khách hàng
                        là thành công của chính mình.
                        <p>
                        Nhà máy sản xuất của chúng tôi được trang bị các thiết bị dây
                        chuyền công nghệ mới nhất, cùng đội ngũ cán bộ, công nhân kỹ
                        thuật lành nghề, sản phẩm chất lượng cao cấp, chủng loại đa
                        dạng về mẫu mã, quy cách, kích thước, màu sắc, luôn sẵn sàng
                        đáp ứng mọi nhu cầu của khách hàng về chất lượng, số lượng và
                        tiến độ giao hàng.
                        </p>
                    </p>
                    <h2 class="fh5co-heading animate-box" data-animate-effect="fadeInLeft">NGÀNH SẢN XUẤT VÀ KINH DOANH</h2>
                    <p class="animate-box" data-animate-effect="fadeInLeft">
                        - Chuyên sản xuất các mặt hàng cốc, nắp, bát, đĩa, khay bánh
                        kẹo, sản phẩm định hình...<br/>
                        - Sản xuất màng PP dùng cho hút định hình. Khổ 600-730mm,
                        độ dày 0.2-1.2mm.<br/>
                        - Nhận các đơn đặt hàng ngành nhựa PP.<br/>
                        - Thương mại các loại vật tư bao bì như PP, PE, PET
                    </p>
                </div>
                {{--<div class="col-md-5 col-md-push-1 animate-box" data-animate-effect="fadeInLeft">--}}
                    {{--<img src="/images/logo.jpg" alt="Free HTML5 Bootstrap Template" class="img-responsive">--}}
                {{--</div>--}}
            </div>

        </div>




        @include('front_end.includes.service')

    </div>
@stop
