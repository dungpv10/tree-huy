@extends('front_end.layouts.main')
@section('title') Sản phẩm chính - Luatdanplastic.com @stop
@section('main_content')
    <div id="fh5co-main">

        @include('front_end.includes.all_product_by_category')
        @include('front_end.includes.service')
    </div>
@stop
