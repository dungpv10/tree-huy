<div class="fh5co-testimonial" style="padding: 0 0 3em 0">
    <div class="main-banner">
        <div class="owl-carousel-fullwidth animate-box" data-animate-effect="fadeInLeft">
            <div class="item"
                 style="width: 100%;background-image: url('/images/banners/banner-1.jpg');background-repeat: no-repeat;position:relative;">
                <p class="text-center quote position-top">
                    &ldquo;Chào mừng đến với Cơ sở sản xuất nhựa Luật Đàn.&rdquo; <cite class="author"><a
                                class="btn btn-primary" style="font-size: 100%;" href="{{ route('front_end.introduction') }}">Giới thiệu thông
                            tin</a> </cite>
                </p>
            </div>
            <div class="item"
                 style="width: 100%;background-image: url('/images/banners/banner-2.jpg');background-repeat: no-repeat;position:relative;">
                <p class="text-center quote quote-square position-top" style="font-weight: 500;background-color: yellow;">
                    <span style="color : #333">Mẫu thiết kế đa dạng </span><br/>
                    <span style="color : #b20000;">Chất lượng uy tín</span><br/>
                    <span style="color: #c9005b">Khách hàng tín nhiệm</span>&rdquo;
                </p>
            </div>
            <div class="item"
                 style="width: 100%;background-image: url('/images/banners/banner-3.jpg');background-repeat: no-repeat;position:relative;">
                <p class="text-center quote quote-circle">
                    &ldquo;Dịch vụ thiết kế logo chuyên nghiệp&rdquo;
                </p>
            </div>
            <div class="item"
                 style="width: 100%;background-image: url('/images/banners/banner-4.jpg');background-repeat: no-repeat;position:relative;">
                <p class="text-center quote position-top">
                    &ldquo;Giá ưu đãi khi đặt hàng số lượng lớn&rdquo; <cite class="author"><a
                                class="btn btn-primary" style="font-size: 100%;" href="{{ route('front_end.products') }}">Danh mục sản phẩm</a> </cite>
                </p>
            </div>
        </div>
    </div>
</div>
