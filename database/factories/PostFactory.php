<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\Models\Post::class, function (Faker $faker) {
    $categoryId = \App\Models\Category::pluck('id')->toArray();
    return [
        'name' => $faker->name,
        'slug' => str_slug($faker->name),
        'thumbnail_path' => $faker->imageUrl(),
        'description' => $faker->paragraph(5),
        'category_id' => array_rand($categoryId)[0],
        'price' => rand(100, 200)
    ];
});
