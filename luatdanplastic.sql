-- MySQL dump 10.13  Distrib 5.7.21, for Linux (x86_64)
--
-- Host: localhost    Database: luatdanplastic
-- ------------------------------------------------------
-- Server version	5.7.21-0ubuntu0.17.10.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categories`
--

LOCK TABLES `categories` WRITE;
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
INSERT INTO `categories` VALUES (1,'Dr. Katrine Kilback MD','gideon-yundt-ii','Quo aut quisquam voluptate est quia voluptatem adipisci. Est ut adipisci dolor quam saepe dolor officia optio. Maxime aliquid qui veniam veritatis voluptas libero natus. Amet aliquam odio est.','2018-03-21 06:56:28','2018-03-23 04:03:22','2018-03-23 04:03:22'),(2,'Doyle Davis','prof-lucious-reinger','Mollitia assumenda iure ipsa omnis et. Dolores omnis alias maxime consectetur ut. Ut debitis harum sed dolor omnis consectetur. Vel beatae corporis quia. Fuga est ea quia similique cupiditate. Non aut dolore consequuntur est cum illum exercitationem.','2018-03-21 06:56:28','2018-03-23 04:03:21','2018-03-23 04:03:21'),(3,'Wilburn Rice','adolph-runte','Eveniet distinctio unde voluptatem eum saepe et libero. Delectus sunt iste quas voluptatem autem rerum labore. Sunt rerum consequatur omnis. Esse a quia magni velit. Et numquam voluptatem placeat quasi unde ut. Perspiciatis quae blanditiis repudiandae ducimus et. Rerum vel in voluptatem quasi qui quasi quia ut.','2018-03-21 06:56:28','2018-03-23 04:03:20','2018-03-23 04:03:20'),(4,'Dusty Connelly','dr-westley-kassulke','Voluptates dolorem consequatur provident praesentium dicta. Accusamus dolor deleniti sit fugiat eveniet deserunt. Nihil in officiis qui. Quam ut hic dicta et maiores odio. Numquam aspernatur modi distinctio iure consectetur. Quam quia voluptatem possimus unde.','2018-03-21 06:56:28','2018-03-23 04:03:19','2018-03-23 04:03:19'),(5,'Samson Koepp','ron-ullrich','Nesciunt voluptate id vel dolorem quos et. Sit occaecati odit debitis recusandae. Quidem architecto inventore quae voluptatum tenetur non ea. Ut est officiis excepturi impedit ratione.','2018-03-21 06:56:28','2018-03-23 04:03:18','2018-03-23 04:03:18'),(6,'Sản phẩm chính','san-pham-chinh','Sản phẩm chính','2018-03-23 04:03:54','2018-03-24 08:15:41',NULL),(7,'Sản phẩm kinh doanh','san-pham-kinh-doanh','Sản phẩm kinh doanh','2018-03-23 07:12:17','2018-03-24 08:15:55',NULL),(8,'Sản phẩm chiến lược','san-pham-chien-luoc','Sản phẩm chiến lược','2018-03-24 08:16:03','2018-03-24 08:16:03',NULL);
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (9,'2014_10_12_000000_create_users_table',1),(10,'2014_10_12_100000_create_password_resets_table',1),(11,'2018_03_19_195327_categories',1),(12,'2018_03_19_195336_posts',1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `posts`
--

DROP TABLE IF EXISTS `posts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `posts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image_url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `category_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `posts`
--

LOCK TABLES `posts` WRITE;
/*!40000 ALTER TABLE `posts` DISABLE KEYS */;
INSERT INTO `posts` VALUES (1,'Lue Dickinson','rebeka-dubuque','https://lorempixel.com/640/480/?19935','Tempora ad et magnam laudantium doloremque molestias. Culpa quasi pariatur praesentium possimus atque id. Explicabo minus velit dolorem vel nostrum aut quasi quae. Qui sapiente occaecati dolorem reiciendis omnis. Exercitationem beatae et deserunt ut consequuntur suscipit est.',1,'2018-03-21 06:56:28','2018-03-23 04:03:22','2018-03-23 04:03:22'),(2,'Dr. Jamaal Schoen Sr.','stacey-ernser','https://lorempixel.com/640/480/?41742','Et eveniet voluptatibus dicta officiis. Totam sunt tempora minus sunt sint quo. Doloremque qui eum beatae beatae perspiciatis qui. Veniam nesciunt error aut molestias illum. Vel aut dolores vero eum velit quod. Et vero molestias sequi sint tempora incidunt rerum. Sunt repellat quo nostrum eum quia excepturi praesentium culpa.',1,'2018-03-21 06:56:28','2018-03-23 04:03:22','2018-03-23 04:03:22'),(3,'Gloria Quitzon','mr-werner-waters','https://lorempixel.com/640/480/?40495','Nostrum laudantium asperiores exercitationem vitae. Odio accusamus hic omnis eveniet officia ea repudiandae. Hic velit facilis dolore molestiae necessitatibus labore unde. Blanditiis ex ipsam voluptatem consectetur vel nemo reprehenderit nemo. Accusantium quia officiis consequatur aspernatur. Eos soluta eius laboriosam in ipsa quaerat. Mollitia voluptatum necessitatibus ullam fugit laudantium.',1,'2018-03-21 06:56:28','2018-03-23 04:03:22','2018-03-23 04:03:22'),(4,'Prof. Vada Hettinger I','ms-arlene-orn','https://lorempixel.com/640/480/?91303','Saepe suscipit amet fuga et. Rerum vero voluptates magnam molestiae qui placeat nostrum. Et molestiae sit et maiores ab est quia. Ut quia nobis qui laborum cum et possimus. Commodi non magni et. Qui ea vel minima sunt laborum labore. Modi dolor quia totam.',2,'2018-03-21 06:56:28','2018-03-23 04:03:21','2018-03-23 04:03:21'),(5,'Xander Jerde','marietta-christiansen','https://lorempixel.com/640/480/?54799','Aut magni quo perspiciatis ducimus dolores. Neque ea blanditiis nesciunt qui aliquam nemo neque. Laborum velit possimus hic quas dolores omnis. Similique nobis porro libero animi. Quia ut et sunt voluptatem.',2,'2018-03-21 06:56:28','2018-03-23 04:03:21','2018-03-23 04:03:21'),(6,'Mr. Jamar Daugherty','marquise-schowalter','https://lorempixel.com/640/480/?74096','Qui minima qui minus mollitia tempore. Voluptatem odio sit deserunt dolor enim impedit. Quas ut enim sit eius nobis sed. Perspiciatis cum est voluptas quos recusandae voluptas aut. A consequatur culpa quo laudantium dicta.',2,'2018-03-21 06:56:28','2018-03-23 04:03:21','2018-03-23 04:03:21'),(7,'Miss Lenore Barton','giuseppe-emmerich','https://lorempixel.com/640/480/?21849','Corporis voluptas sint sed non. Aliquid doloribus earum dolorem est. Itaque sunt voluptas fuga sint repellendus. Provident blanditiis rem voluptatibus nostrum. Fugit ad alias aut sapiente doloremque a. Aut qui ut quia quia dicta sed. Voluptate aperiam sed pariatur quod consequatur laborum.',3,'2018-03-21 06:56:28','2018-03-23 04:03:20','2018-03-23 04:03:20'),(8,'Isadore Hirthe','mr-deron-gleason-i','https://lorempixel.com/640/480/?52749','Temporibus ipsa ut inventore. Placeat molestiae ut iste. Repudiandae sit sit in consequatur quia reprehenderit quia aliquam. Iste neque qui esse deleniti porro beatae blanditiis distinctio.',3,'2018-03-21 06:56:28','2018-03-23 04:03:20','2018-03-23 04:03:20'),(9,'Euna Gibson','mr-vicente-denesik-dvm','https://lorempixel.com/640/480/?48147','A qui velit at et est et et molestias. Et recusandae unde temporibus repellat libero aut sit. Animi est animi ut nemo quam aliquid et nostrum. Sed eos dignissimos autem sed aut.',3,'2018-03-21 06:56:28','2018-03-23 04:03:20','2018-03-23 04:03:20'),(10,'Garfield Leffler V','mr-jovanny-kovacek-iv','https://lorempixel.com/640/480/?94361','Cupiditate numquam ipsam et numquam voluptatem libero non. Neque minima velit quo maiores consequuntur. Sequi nihil et voluptatem nam animi cum. Alias et repellat quisquam ut. Sed quaerat autem qui unde omnis. Est rem non repellendus odio.',4,'2018-03-21 06:56:28','2018-03-23 04:03:19','2018-03-23 04:03:19'),(11,'Kylie Romaguera','ms-kelsie-brekke-dvm','https://lorempixel.com/640/480/?84344','Unde autem et aut sed saepe. Incidunt voluptate voluptatum facilis nemo expedita sit. Saepe veritatis beatae qui quia dolor et. Voluptas architecto dolores et nam veniam. Iure sint nam est est qui.',4,'2018-03-21 06:56:28','2018-03-23 04:03:19','2018-03-23 04:03:19'),(12,'Prof. Jaylin O\'Conner V','martina-okon','https://lorempixel.com/640/480/?54745','Autem reiciendis eum porro amet. Qui temporibus veritatis quam et. Numquam consequatur ab quas dolor. Ex et soluta placeat minus cumque voluptatem cum. Maxime dicta incidunt repellendus aut dolorem veniam dolores exercitationem. Est est odit est id quis assumenda.',4,'2018-03-21 06:56:28','2018-03-23 04:03:19','2018-03-23 04:03:19'),(13,'Caterina Jast','mr-conrad-rogahn','https://lorempixel.com/640/480/?84191','Molestias eum ipsum aut distinctio. Et culpa saepe veniam eveniet debitis quis non. Omnis commodi sit aut numquam perspiciatis et nisi illo. Quibusdam corrupti inventore ut et velit laboriosam quod. Molestiae quos est molestias quaerat. Et quas impedit quia eveniet praesentium minima animi. Et saepe maiores pariatur ea sequi.',5,'2018-03-21 06:56:28','2018-03-23 04:03:18','2018-03-23 04:03:18'),(14,'Nova Grant','einar-cummings','https://lorempixel.com/640/480/?11110','Praesentium eum velit eius cumque. Veniam laudantium dolores aliquid voluptate vel necessitatibus. Maxime eum atque laudantium aut ratione perferendis qui. Reiciendis libero qui non aut consectetur. Fugiat velit enim qui. Magnam libero quia et numquam repellat qui.',5,'2018-03-21 06:56:28','2018-03-23 04:03:18','2018-03-23 04:03:18'),(15,'Mr. Brayan O\'Keefe PhD','geovany-lindgren','https://lorempixel.com/640/480/?57282','Beatae veniam dolores sint molestiae nemo rerum ut. Laboriosam delectus voluptatem nam. Vitae quaerat aperiam accusantium quo quis. Aut hic non et necessitatibus. Officiis asperiores modi et id et soluta. Sit ipsum quia deserunt rerum minus ipsam aspernatur. Enim numquam provident accusantium officiis at sed.',5,'2018-03-21 06:56:28','2018-03-23 04:03:18','2018-03-23 04:03:18'),(16,'test','test','uploads/posts/test-79738.png','test',6,'2018-03-23 04:35:38','2018-03-23 04:36:18','2018-03-23 04:36:18'),(17,'Cốc nhựa sử dụng 1 lần - 700ml','coc-nhua-su-dung-1-lan-700ml','uploads/posts/coc-nhua-su-dung-1-lan-700ml-79776.jpg','- Chất liệu: PP\r\n- Dung tích: 700ml\r\n- Màu sắc: Trong suốt\r\n- Quy cách đóng gói: 50 cái/bao',6,'2018-03-23 04:36:16','2018-03-26 01:33:13','2018-03-26 01:33:13'),(18,'Cốc nhựa sử dụng 1 lần - 500ml','coc-nhua-su-dung-1-lan-500ml','uploads/posts/coc-nhua-su-dung-1-lan-500ml-88819.jpg','- Chất liệu: PP \r\n- Dung tích: 500ml \r\n- Màu sắc: Trong suốt \r\n- Quy cách đóng gói: 50 cái/bao',6,'2018-03-23 07:06:59','2018-03-26 01:33:12','2018-03-26 01:33:12'),(19,'Cốc nhựa sử dụng 1 lần - 400ml','coc-nhua-su-dung-1-lan-400ml','uploads/posts/coc-nhua-su-dung-1-lan-400ml-88852.jpg','- Chất liệu: PP \r\n- Dung tích: 700ml \r\n- Màu sắc: Trong suốt \r\n- Quy cách đóng gói: 50 cái/bao',6,'2018-03-23 07:07:32','2018-03-26 01:33:10','2018-03-26 01:33:10'),(20,'Khay thực phẩm','khay-thuc-pham','uploads/posts/khay-thuc-pham-88972.jpg','Khay đựng thực phẩm, bánh kẹo...',6,'2018-03-23 07:09:32','2018-03-24 08:16:51','2018-03-24 08:16:51'),(21,'Màng PP dùng cho hút định hình','mang-pp-dung-cho-hut-dinh-hinh','uploads/posts/mang-pp-dung-cho-hut-dinh-hinh-89164.jpg','Khổ 600-730mm, độ dày 0.2-1.2mm.',6,'2018-03-23 07:12:44','2018-03-24 11:01:05','2018-03-24 11:01:05'),(22,'Khay thực phẩm','khay-thuc-pham','uploads/posts/khay-thuc-pham-89234.jpg','Sản xuất các loại khay đựng thực phẩm, bánh kẹo',6,'2018-03-23 07:13:54','2018-03-24 08:16:47','2018-03-24 08:16:47'),(23,'Các loại vật tư bao bì PP, PE, PET','cac-loai-vat-tu-bao-bi-pp-pe-pet','uploads/posts/cac-loai-vat-tu-bao-bi-pp-pe-pet-89325.jpg','Thương mại các loại vật tư bao bì như PP, PE, PET',7,'2018-03-23 07:15:25','2018-03-24 08:19:01','2018-03-24 08:19:01'),(24,'Hộp đựng cơm, thực phẩm','hop-dung-com-thuc-pham','uploads/posts/hop-dung-com-thuc-pham-89450.jpg','Chất liệu: PET-H, PP - Màu sắc: Trong suốt - Quy cách đóng gói: 50 hộp/bao',6,'2018-03-23 07:17:30','2018-03-24 08:16:42','2018-03-24 08:16:42'),(25,'Chén - Bát','chen-bat','uploads/posts/chen-bat-89540.jpg','- Chất liệu: PP\r\n- Quy cách đóng gói: 50 cái / bao\r\n- Màu sắc: Trắng đục',6,'2018-03-23 07:19:00','2018-03-24 08:16:41','2018-03-24 08:16:41'),(26,'Các loại Đĩa','cac-loai-dia','uploads/posts/cac-loai-dia-89583.jpg','- Chất liệu: PET\r\n- Màu sắc: Trắng đục',6,'2018-03-23 07:19:43','2018-03-24 08:16:39','2018-03-24 08:16:39'),(27,'Cuộn màng dập','cuon-mang-dap','uploads/posts/cuon-mang-dap-79530.png','Màng dán cốc, ly',7,'2018-03-24 08:18:50','2018-03-24 08:18:50',NULL),(28,'Máy dập màng tự động','may-dap-mang-tu-dong','uploads/posts/may-dap-mang-tu-dong-79772.jpg','Máy dập màng ly, cốc tự động',7,'2018-03-24 08:22:52','2018-03-24 08:23:09',NULL),(29,'Túi nilong','tui-nilong','uploads/posts/tui-nilong-79857.jpg','Túi nilong',7,'2018-03-24 08:24:17','2018-03-24 08:26:18',NULL),(30,'Nắp dành cho cốc, ly nhựa','nap-danh-cho-coc-ly-nhua','uploads/posts/nap-danh-cho-coc-ly-nhua-79966.jpg','Nắp dành cho cốc, ly nhựa',6,'2018-03-24 08:26:06','2018-03-26 01:33:08','2018-03-26 01:33:08'),(31,'Khay Trứng','khay-trung','uploads/posts/khay-trung-80086.jpeg','Khay Trứng',8,'2018-03-24 08:28:06','2018-03-24 08:28:06',NULL),(32,'Khay định hình','khay-dinh-hinh','uploads/posts/khay-dinh-hinh-80166.JPG','Khay định hình',8,'2018-03-24 08:29:26','2018-03-24 08:30:08',NULL),(33,'Khay điện tử','khay-dien-tu','uploads/posts/khay-dien-tu-80227.jpg','Khay điện tử',8,'2018-03-24 08:30:27','2018-03-24 08:30:27',NULL),(34,'Cốc nhựa sử dụng 1 lần - 400ml ngắn','coc-nhua-su-dung-1-lan-400ml-ngan','uploads/posts/coc-nhua-su-dung-1-lan-400ml-ngan-89118.jpg','Cốc nhựa sử dụng 1 lần - 400ml ngắn',6,'2018-03-24 10:58:38','2018-03-26 01:33:05','2018-03-26 01:33:05'),(35,'Cốc nhựa sử dụng 1 lần - 300ml','coc-nhua-su-dung-1-lan-300ml','uploads/posts/coc-nhua-su-dung-1-lan-300ml-89143.jpg','Cốc nhựa sử dụng 1 lần - 300ml',6,'2018-03-24 10:59:03','2018-03-26 01:33:04','2018-03-26 01:33:04'),(36,'Cốc nhựa sử dụng 1 lần - 280ml','coc-nhua-su-dung-1-lan-280ml','uploads/posts/coc-nhua-su-dung-1-lan-280ml-89160.jpg','Cốc nhựa sử dụng 1 lần - 280ml',6,'2018-03-24 10:59:20','2018-03-26 01:33:03','2018-03-26 01:33:03'),(37,'Cốc nhựa sử dụng 1 lần - 220ml','coc-nhua-su-dung-1-lan-220ml','uploads/posts/coc-nhua-su-dung-1-lan-220ml-89175.jpg','Cốc nhựa sử dụng 1 lần - 220ml',6,'2018-03-24 10:59:35','2018-03-26 01:33:02','2018-03-26 01:33:02'),(38,'Thìa nhựa sử dụng 1 lần','thia-nhua-su-dung-1-lan','uploads/posts/thia-nhua-su-dung-1-lan-89995.jpg','Thìa nhựa sử dụng 1 lần',6,'2018-03-24 11:13:15','2018-03-26 01:33:00','2018-03-26 01:33:00'),(39,'Cốc nhựa sử dụng 1 lần - 500ml','coc-nhua-su-dung-1-lan-500ml','uploads/posts/coc-nhua-su-dung-1-lan-500ml-28187.jpg','Cốc nhựa sử dụng 1 lần - 500ml',6,'2018-03-26 01:36:27','2018-03-26 01:44:04','2018-03-26 01:44:04'),(40,'Cốc nhựa sử dụng 1 lần - 400ml','coc-nhua-su-dung-1-lan-400ml','uploads/posts/coc-nhua-su-dung-1-lan-400ml-28200.jpg','Cốc nhựa sử dụng 1 lần - 400ml',6,'2018-03-26 01:36:40','2018-03-26 01:44:03','2018-03-26 01:44:03'),(41,'Cốc nhựa sử dụng 1 lần - 300ml','coc-nhua-su-dung-1-lan-300ml','uploads/posts/coc-nhua-su-dung-1-lan-300ml-28221.jpg','Cốc nhựa sử dụng 1 lần - 300ml',6,'2018-03-26 01:37:01','2018-03-26 01:44:02','2018-03-26 01:44:02'),(42,'Cốc nhựa sử dụng 1 lần - 500ml','coc-nhua-su-dung-1-lan-500ml','uploads/posts/coc-nhua-su-dung-1-lan-500ml-28660.jpg','Cốc nhựa sử dụng 1 lần - 500ml',6,'2018-03-26 01:44:20','2018-03-26 01:44:20',NULL),(43,'Cốc nhựa sử dụng 1 lần - 400ml','coc-nhua-su-dung-1-lan-400ml','uploads/posts/coc-nhua-su-dung-1-lan-400ml-28673.jpg','Cốc nhựa sử dụng 1 lần - 400ml',6,'2018-03-26 01:44:33','2018-03-26 01:44:33',NULL),(44,'Cốc nhựa sử dụng 1 lần - 300ml','coc-nhua-su-dung-1-lan-300ml','uploads/posts/coc-nhua-su-dung-1-lan-300ml-28783.jpg','Cốc nhựa sử dụng 1 lần - 300ml',6,'2018-03-26 01:46:23','2018-03-26 01:46:23',NULL),(45,'Cốc nhựa sử dụng 1 lần - 280ml','coc-nhua-su-dung-1-lan-280ml','uploads/posts/coc-nhua-su-dung-1-lan-280ml-28798.jpg','Cốc nhựa sử dụng 1 lần - 280ml',6,'2018-03-26 01:46:38','2018-03-26 01:46:38',NULL),(46,'Cốc nhựa sử dụng 1 lần - 220ml','coc-nhua-su-dung-1-lan-220ml','uploads/posts/coc-nhua-su-dung-1-lan-220ml-28812.jpg','Cốc nhựa sử dụng 1 lần - 220ml',6,'2018-03-26 01:46:52','2018-03-26 01:46:52',NULL),(47,'Cốc nhựa sử dụng 1 lần - 140ml','coc-nhua-su-dung-1-lan-140ml','uploads/posts/coc-nhua-su-dung-1-lan-140ml-28830.jpg','Cốc nhựa sử dụng 1 lần - 140ml',6,'2018-03-26 01:47:10','2018-03-26 01:47:10',NULL),(48,'Nắp dành cho cốc nhựa 1 lần - Ø85','nap-danh-cho-coc-nhua-1-lan-o85','uploads/posts/nap-danh-cho-coc-nhua-1-lan-o85-28874.jpg','Nắp dành cho cốc nhựa 1 lần - Ø85',6,'2018-03-26 01:47:54','2018-03-26 01:47:54',NULL),(49,'Nắp dành cho cốc nhựa 1 lần - Ø95','nap-danh-cho-coc-nhua-1-lan-o95','uploads/posts/nap-danh-cho-coc-nhua-1-lan-o95-28886.jpg','Nắp dành cho cốc nhựa 1 lần - Ø95',6,'2018-03-26 01:48:06','2018-03-26 01:48:06',NULL),(50,'Cốc nhựa sử dụng 1 lần - 700ml','coc-nhua-su-dung-1-lan-700ml','uploads/posts/coc-nhua-su-dung-1-lan-700ml-37592.jpg','Cốc nhựa sử dụng 1 lần - 700ml',6,'2018-03-26 04:13:12','2018-03-26 04:13:12',NULL);
/*!40000 ALTER TABLE `posts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'admin','admin@admin.com','$2y$10$SIgRZUplH8t6u18FnUmCw.oJJtTWgoI0fWKNfdeokQiXNfiTgHwq.','i22req2xOxODdbTFrgIVZV0XNIJYmUBhxpunWNwMQl8BHdw13evmoBhxgQ99',NULL,NULL);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-03-26  9:41:32
